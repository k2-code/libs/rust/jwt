use jwt_simple::prelude::{Claims, RS256KeyPair, RS256PublicKey, RSAKeyPairLike, RSAPublicKeyLike};
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use std::env;
use std::fmt::Debug;

pub use jwt_simple::prelude::{Duration, JWTClaims};

pub type JWTResult<T> = Result<T, Box<dyn std::error::Error>>;

#[derive(Serialize, Deserialize, Debug)]
pub struct UserClaims {
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub slug: Option<String>,
}

impl UserClaims {
    pub fn id(id: impl Into<String>) -> Self {
        Self {
            id: Some(id.into()),
            slug: None,
        }
    }

    pub fn id_slug(id: impl Into<String>, slug: impl Into<String>) -> Self {
        Self {
            id: Some(id.into()),
            slug: Some(slug.into()),
        }
    }
}

pub fn generate<CustomClaims>(claims: CustomClaims, duration: Option<Duration>) -> JWTResult<String>
where
    CustomClaims: Serialize + DeserializeOwned + Debug,
{
    let private_key: RS256KeyPair = get_private_key()?;
    let claims: JWTClaims<CustomClaims> =
        Claims::with_custom_claims(claims, duration.unwrap_or_else(|| Duration::from_days(30)));

    Ok(private_key.sign(claims)?)
}

pub fn verify<CustomClaims>(token: &str) -> JWTResult<JWTClaims<CustomClaims>>
where
    CustomClaims: Serialize + DeserializeOwned + Debug,
{
    let public_key: RS256PublicKey = get_public_key()?;
    let claims: JWTClaims<CustomClaims> = public_key.verify_token::<CustomClaims>(token, None)?;

    Ok(claims)
}

fn get_private_key() -> JWTResult<RS256KeyPair> {
    Ok(RS256KeyPair::from_pem(&String::from_utf8(
        base64::decode(env::var("JWT_PRIVATE_KEY")?)?,
    )?)?)
}

fn get_public_key() -> JWTResult<RS256PublicKey> {
    Ok(RS256PublicKey::from_pem(&String::from_utf8(
        base64::decode(env::var("JWT_PUBLIC_KEY")?)?,
    )?)?)
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_claims() -> UserClaims {
        UserClaims::id("test_id")
    }

    #[test]
    fn can_get_user_claims_by_id() {
        assert!(UserClaims::id("test_id").id.is_some());
    }

    #[test]
    fn can_get_user_claims_by_id_slug() {
        let claims: UserClaims = UserClaims::id_slug("test_id", "test_slug");

        assert!(claims.id.is_some());
        assert!(claims.slug.is_some());
    }

    #[test]
    fn can_get_private_key() {
        assert!(get_private_key().is_ok());
    }

    #[test]
    fn can_get_public_key() {
        assert!(get_public_key().is_ok());
    }

    #[test]
    fn can_generate() {
        assert!(generate(get_claims(), None).is_ok());
    }

    #[test]
    fn can_generate_with_custom_duration() {
        assert!(generate(get_claims(), Some(Duration::from_hours(1))).is_ok());
    }

    #[test]
    fn can_verify() {
        let token: String = generate(get_claims(), None).unwrap();

        assert!(verify::<UserClaims>(&token).is_ok());
    }
}
